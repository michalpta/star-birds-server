const express = require('express');
const http = require('http');
const ws = require('ws');
const { interval } = require('rxjs');
const ships = [];
const app = express()
app.get('/', function (req, res) {
  res.send('<div style="font-family: sans-serif; text-align: center; margin: 50px;">+=== Star Birds Server v 1.0 ===+</div>')
})
const server = http.createServer(app);
const wss = new ws.Server({ server });
interval(1000/60).subscribe(() => {
  wss.clients.forEach(function each(client) {
    if (client.readyState === ws.OPEN) {
      client.send(JSON.stringify(ships));
    }
  });
});
wss.on('connection', function connection(ws) {
  console.log('* new connection established');
  ws.send(JSON.stringify('Welcome to Star Birds Server!'));
  ws.on('message', function incoming(message) {
    const newShip = JSON.parse(message);
    let shipToUpdate = ships.find(s => s.nick === newShip.nick);
    if (!shipToUpdate) {
      shipToUpdate = newShip
      ships.push(newShip);
    }
    shipToUpdate.x = newShip.x;
    shipToUpdate.y = newShip.y;
    shipToUpdate.rotation = newShip.rotation;
  });
});
server.listen(process.env.PORT || 81)
console.log('                          ');
console.log(' Star Birds Server v 1.0  ');
console.log('+========================+');
console.log(' server started...        ');
console.log('                          ');